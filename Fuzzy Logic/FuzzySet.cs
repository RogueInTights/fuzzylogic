﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuzzy_Logic
{
    class FuzzySet
    {
        private List<FuzzyItem> body = new List<FuzzyItem>();
        private string title;

        private static int accuracy = 2; // Число десятичных разрядов степени принадлежности
        private static List<FuzzySet> instances = new List<FuzzySet>(); // Все множества

        public FuzzySet()
        {
            instances.Add(this);
        }

        public FuzzySet(List<FuzzyItem> set)
        {
            foreach (FuzzyItem item in set)
            {
                this.body.RemoveAll(element => element.Value == item.Value);
                this.body.Add(item);
            }

            instances.Add(this);
        }

        public FuzzySet(List<FuzzyItem> set, string title)
        {
            foreach(FuzzyItem item in set)
            {
                this.body.RemoveAll(element => element.Value == item.Value);
                this.body.Add(item);
            }

            this.title = title;

            instances.Add(this);
        }

        // Члены множества:
        public List<FuzzyItem> Body
        {
            get
            {
                return this.body;
            }
        }

        // Заголовок множества:
        public string Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.title = value;
            }
        }

        // Точность степеней принадлежности для всех экземпляров класса:
        public static int Accuracy
        {
            get
            {
                return accuracy;
            }

            set
            {
                /* Исправить */

                try
                {
                    accuracy = (int)value;
                }
                catch
                {
                    throw new Exception("Invalid input");
                }

                //accuracy = value;

                foreach (FuzzySet set in instances)
                {
                    FuzzySet.Round(set);
                }
            }
        }

        // Добавление элемента к нечеткому множеству:
        public void Add(FuzzyItem item)
        {
            this.body.RemoveAll(element => element.Value == item.Value);
            this.body.Add(item);
        }

        // Выборка элементов из нечеткого множества:
        public FuzzySet Selection(Predicate<FuzzyItem> predicate)
        {
            List<FuzzyItem> set = new List<FuzzyItem>();

            foreach(FuzzyItem item in this.body)
            {
                if (predicate(item))
                {
                    set.Add(item);
                }
            }

            return new FuzzySet(set);
        }

        // Дополнение нечеткого множества:
        public static FuzzySet operator !(FuzzySet set)
        {
            FuzzySet complement = new FuzzySet();

            foreach (FuzzyItem item in set.Body)
            {
                complement.Add(new FuzzyItem(item.Value, 1 - item.Degree));
            }

            return complement;
        }

        // Объединение нечетких множеств:
        public static FuzzySet operator |(FuzzySet set1, FuzzySet set2)
        {
            FuzzySet union = new FuzzySet();

            foreach (FuzzyItem item1 in set1.Body)
            {
                FuzzyItem item2 = set2.Body.Find(item => item.Value == item1.Value);

                if (item2 != null)
                {
                    union.Add((item1.Degree >= item2.Degree) ? item1 : item2);
                }
                else
                {
                    union.Add(item1);
                }
            }

            foreach (FuzzyItem item2 in set2.Body)
            {
                FuzzyItem item1 = set1.Body.Find(item => item.Value == item2.Value);

                if (item1 == null)
                {
                    union.Add(item2);
                }
            }

            return union;
        }

        // Пересечение нечетких множеств:
        public static FuzzySet operator &(FuzzySet set1, FuzzySet set2)
        {
            FuzzySet intersection = new FuzzySet();

            foreach (FuzzyItem item1 in set1.Body)
            {
                FuzzyItem item2 = set2.Body.Find(item => item.Value == item1.Value);

                if (item2 != null)
                {
                    intersection.Add((item1.Degree <= item2.Degree) ? item1 : item2);
                }
            }

            return intersection;
        }

        // Возведение нечеткого множества в степень (концентрация | растяжение):
        public static FuzzySet Power(FuzzySet set, double index)
        {
            FuzzySet result = new FuzzySet();

            foreach (FuzzyItem item in set.Body)
            {
                result.Add(new FuzzyItem(item.Value, Math.Pow(item.Degree, index)));
            }

            return result;
        }

        // Округление степеней принадлежности до заданной точности:
        private static void Round(FuzzySet set)
        {
            foreach (FuzzyItem item in set.Body)
            {
                item.Degree = Math.Round(item.Degree, accuracy);
            }
        }
    }
}