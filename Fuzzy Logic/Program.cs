﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuzzy_Logic
{
    class Program
    {
        // Функция логгирования:
        private static void Log(FuzzySet set, string title = "Заголовок")
        {
            Console.WriteLine(' ');
            Console.WriteLine("--------- " + ((set.Title != null) ? set.Title : title) + " ---------");
            Console.WriteLine(' ');

            foreach (FuzzyItem item in set.Body)
            {
                Console.WriteLine(item.Value + ':');
                Console.WriteLine(item.Degree.ToString());

                Console.WriteLine(' ');
            }
        }

        static void Main(string[] args)
        {
            // Виды медведей:
            string[] keys = new string[] {
                "Бурый медведь",
                "Медведь гриззли",
                "Белый медведь",
                "Гималайский медведь",
                "Пещерный медведь",
                "Панда"
            };

            // Набор данных для нечеткого множества "Опасные медведи":
            List<FuzzyItem> set = new List<FuzzyItem>();
            for (int i = 0; i < keys.Length; i++)
            {
                set.Add(new FuzzyItem(keys[i], i * .1 + .3));
            }

            // Набор данных для нечеткого множества "Страшные медведи":
            List<FuzzyItem> set2 = new List<FuzzyItem>();
            Random rand = new Random();
            foreach (string key in keys)
            {           
                double someNumber = rand.Next(0, 10) * .1;

                set2.Add(new FuzzyItem(key, someNumber));
            }

            FuzzySet dangerousBears = new FuzzySet(set);
            dangerousBears.Add(new FuzzyItem("Коала", 1));

            FuzzySet scaryBears = new FuzzySet(set2);
            FuzzySet mostDangerousBears = dangerousBears.Selection(bear => bear.Degree > .5);
            FuzzySet notDangerousBears = !dangerousBears;
            FuzzySet union = dangerousBears | scaryBears;
            FuzzySet intersection = dangerousBears & scaryBears;
            FuzzySet veryScaryBears = FuzzySet.Power(scaryBears, 2); // Усиление нечеткости (концентрация)
            FuzzySet notVeryScaryBears = FuzzySet.Power(scaryBears, .5); // Ослабление нечеткости (растяжение)

            FuzzySet.Accuracy = 3;

            /* Вывод */

            dangerousBears.Title = "Опасные медведи";
            scaryBears.Title = "Страшные медведи";

            // Log(set: dangerousBears);
            Log(set: scaryBears);
            Log(veryScaryBears, "Очень страшные медведи");
            Log(notVeryScaryBears, "Не очень страшные медведи");
            // Log(mostDangerousBears, "Самые опасные медведи (> .5)");
            // Log(notDangerousBears, "Не опасные медведи");
            // Log(union, "Опасные или страшные медведи");
            // Log(intersection, "Опасные и страшные медведи");


            Console.ReadKey();

            /*
                1. Дополнить функциональность нечетких множеств (усиление нечеткости и т.д.)
                2. Запилить синтаксический анализатор и класс FuzzyLogic, в котором он будет реализован. 
                    Принцип действия:

                    fuzzySet1.Title = "Вкусный фрукт"
                    fuzzySet2.Title = "Экзотический фрукт"

                    FuzzyLogic logic = new FuzzyLogic(fuzzySet1, fuzzySet2, "экзотический или вкусный фрукт");

                    logic.Result // вернет наиболее подходящий результат из объединения нечетких множеств
             */
        }
    }
}
