﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuzzy_Logic
{
    class FuzzyItem
    {
        private string value;
        private double degree;

        public FuzzyItem(string value, double degree)
        {
            this.value = value;
            this.degree = (degree < 0) ? 0 : (degree > 1) ? 1 : degree; 
        }

        public string Value
        {
            get
            {
                return this.value;
            }
        }

        public double Degree
        {
            get
            {
                return this.degree;
            }

            set
            {
                this.degree = (value < 0) ? 0 : (value > 1) ? 1 : value;
            }
        }
    }
}
